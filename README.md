# test

This is a test for Containers of responsive web design on a somewhat controlled browser scheme. 

Coinsiderations:

- Python 3
- Requires PyQt5 (via pip)
- PyQtWebEngine (via pip)
- tested on pyqt5-5.12.1 and PyQtWebEngine-5.12.1 

For DB test

- mongodb 3.6.3 
- pymongo-3.7.2
- mongoengine 3.7.2

The intention is to work on sizes and add a status bar at the bottom with simple desktop widget for offline reference.
This is a proof of concept. a Mock up of what could be.

- Info
  Office ID
  Terminal ID
  Connection Status with server
  Server Connected to (local Address vs Remote)


