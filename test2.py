def mainPyQt5():
    # ?????????import
    import sys
    from PyQt5.QtWidgets import QApplication
    from PyQt5.QtCore import QUrl
    from PyQt5.QtWebEngineWidgets import QWebEngineView

    url = 'http://ghala.arubaairlines.com/login'

    app = QApplication(sys.argv)

    # QWebEngineView???Web?????
    browser = QWebEngineView()
    browser.load(QUrl(url))
    browser.resize(800, 700)
    browser.show()

    sys.exit(app.exec_()) 

if __name__ == '__main__':
    mainPyQt5()
