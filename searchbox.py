import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QCompleter, QPushButton, QAction, QLineEdit, QMessageBox, QLabel
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot, QTimer
from testmodule import * 

#App simple para buscar Agencias - Mock Up de Pantalla basica

class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = " Buscar Agencia"
        self.left = 20
        self.top = 20
        self.width = 300
        self.height = 140
        self.setFixedSize(self.width, self.height)
        self.initUI()
    
    def list_emails(self):
        listoemails = []
        searchemails = Vendor.objects()
        for i in searchemails:
            listoemails.append(i.email)
        return listoemails
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.textbox = QLineEdit(self)
        self.textbox.move(20,20)
        self.textbox.resize(260,20)
        self.textbox.returnPressed.connect(self.on_click)
        self.label1 = QLabel(self)
        self.label1.resize(260,20)
        self.label1.move(20,40)
        self.label2 = QLabel(self)
        self.label2.resize(260,20)
        self.label2.move(20,60)
        self.label3 = QLabel(self)
        self.label3.resize(260,20)
        self.label3.move(20,80)
        listoemails = []
        autoComplete = QCompleter(self.list_emails(),self.textbox)
        self.textbox.setCompleter(autoComplete)
        self.textbox.show()
        self.button = QPushButton('Buscar Agencia',self)
        self.button.move(20,100)
        self.button.resize(150,30)
        self.button.clicked.connect(self.on_click)
        self.show()

    @pyqtSlot()

    def checkandsearch(self):
        textboxValue = self.textbox.text()
        if len(textboxValue)>=3:
            vndrresult = Vendor.objects(email__icontains=textboxValue)
            return vndrresult

    def on_click(self):
        textboxValue = self.textbox.text()
        find_vendor = self.checkandsearch()
        if not find_vendor:
            QMessageBox.information(self, 'No Records', "No Result", QMessageBox.Ok, QMessageBox.Ok)
        else:
            QMessageBox.information(self, 'La agencia es', "Es: " + find_vendor[0].name, QMessageBox.Ok, QMessageBox.Ok)
            self.label1.setText("Busqueda:" + textboxValue)
            self.label2.setText("Email:" + find_vendor[0].email)
            self.label3.setText("Agencia:" + find_vendor[0].name)
            self.textbox.setText("")
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())

